# Flask Sample App
- [Flask Sample App](#flask-sample-app)
  - [Pre-requisite](#pre-requisite)
  - [Architecture \& Command lines](#architecture--command-lines)
  - [Configuration](#configuration)
  - [Endpoints](#endpoints)
  - [To Do \& Questions](#to-do--questions)
    - [Build](#build)
    - [Deployment](#deployment)

## Pre-requisite

To run locally application, install `docker` and `docker compose`.

Run `docker compose up -d` to start app.

Check the following endpoints:
```console
$ curl localhost:8080/-/healthy -w "%{http_code}"
{}
200
$ curl localhost:8080/-/ready -w "%{http_code}"
{}
200
$ curl localhost:8080/user -w "%{http_code}"
[...]
200
```

## Architecture & Command lines

Application is made of:
* a Web Server for API Calls. The web server can be started with `flask run --host 0.0.0.0 --port 8080`
* migration script to confugure the database. The migration script can be run with `flask db migrate`

Application needs a PostgreSQL database to run.

## Configuration

The following environment variable allow to configure application:
* `POSTGRES_USERNAME`: username of PostgreSQL
* `POSTGRES_PASSWORD`: password of PostgreSQL
* `POSTGRES_HOST`: hostname or IP of PostgreSQL
* `POSTGRES_PORT`: listening port of PostgreSQL
* `POSTGRES_DATABASE`: name of the database

## Endpoints

The following endpoints are available
* `GET localhost:8080/-/healthy`: return `200` when server is up
* `GET localhost:8080/-/ready`: return `200` when server is up
* `GET localhost:8080/user`: return list of users in database
* `POST localhost:8080/user`: create a user in database. The request must contain a body: `{'username': 'my_username', 'email': 'my_email'}`

## To Do & Questions

### Build

* Create a GitLab CI pipeline that build the docker image.
* Suggest improvement of this pipeline.
* Suggest optimizations on the Dockerfile

### Deployment

* Create Helm chart templates to deploy application
  * A minimum of 2 pods are required for web server
  * don't forget to run migration scripts at deployment
  * After installation, the endpoint `/user` works well.
* Are liveness & readiness probes correctly implemented? Why?
* What improvement can be done in your Helm Chart?

remarque 

his database is not linked de the project , 

* Create a GitLab CI pipeline that build the docker image.
  
* Suggest improvement of this pipeline.
  - have multi pipeline project , 
    - dev (feature): 
       - analize code pefore build on dev branch,
       - scan images and vulnerabilites ,
       - dont need to retain images builded ,
    - dev (integration )
       - non regression tests 
       - create arfefact release of code packaging on homol , 
       - retain builded images ( condidate snapshot)
    - master (prod/hom) : 
       - deploy on homol 
       - test end to end 
       - charge and stress test (autoscaling)
       - go prod manual action is required 
  - other impovement can be used , as dind or kaniko (better solution ), the documentation is on gitlab , 
* Suggest optimizations on the Dockerfile
  - to build more rapidly we can external the libs in a mounted permeneant volume to gain more time on building images 
  - or use pre-builded machine that install needed packages like
  ```
  RUN apk add --no-cache --virtual .build-deps \
        gcc \
        libc-dev \
        libffi-dev \
        openssl-dev \
        make \
    && pip install --upgrade pip \ 
    && pip install --no-cache-dir poetry==1.1.13  
  ```
  - improve security with using non root user 
  - have multi folder folder per app , it is a good practice to isolate , in the case of multi-container 


  * install helm chart pg 
  ```
  Add Chart Repository to Helm

helm repo add bitnami https://charts.bitnami.com/bitnami

Install Chart

helm install my-postgresql bitnami/postgresql --version 11.1.26
```
* pakaging the helm chart 
db
https://gitlab.com/gitops-demo/apps/my-python-app4

* Create Helm chart templates to deploy application (done)
  * A minimum of 2 pods are required for web server (done)
  * don't forget to run migration scripts at deployment (can be runned with the exec.docker-cmd )
  * After installation, the endpoint `/user` works well. ( not tested )
* Are liveness & readiness probes correctly implemented? Why? ( yes , need to verify autoscaling rules )
* What improvement can be done in your Helm Chart? 
  - add security layers as pcp and network policies 
  - add admin security helm chart to prepere landing zone invirement , 
  - isolating component db/app 