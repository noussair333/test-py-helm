#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
   CREATE TABLE user (
   id datatype(length) primary key ,
   username datatype(length) ,
   email datatype(length) ,
   table_constraints
);
EOSQL